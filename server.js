const app = require('express')()
const bodyParser = require('body-parser')
const cors = require('cors')

const inspect = require('util').inspect

const fs = require('fs')
const youtubedl = require('youtube-dl')

const youtubeUrlPattern = /^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$/

app.use(cors())
app.use(bodyParser.json())

app.post('/api/video', (req, res) => {
  const body = req.body
  
  if (!body || !body.url) {
    res.sendStatus(400)
  } else {
    const {url} = body
    
    if (!youtubeUrlPattern.test(url)) {
      res.sendStatus(400)
    }

    youtubedl.getInfo(url, ['--youtube-skip-dash-manifest'], (err, info) => {
      if (err) {
        res.sendStatus(500)
      }

      const {uploader, title, thumbnail, formats} = info
      // All video formats for download (skipping audio)
      videoFormats = formats
        .filter(format => format.format_note !== 'DASH audio' && format.filesize && format.ext === 'mp4') 
        .map(format => ({
          id: format.format_id, 
          note: format.format_note, 
          url: format.url
        }))

      res.json({
        uploader, 
        title, 
        thumbnail,
        formats: videoFormats,
      })
    })     
  }
})

app.listen(5000, console.log('Running server at Port 5000'))