import React, { Component } from 'react';
import './App.css';

import TextField from '@atlaskit/field-text'
import Button from '@atlaskit/button'
import PageHeader from '@atlaskit/page-header'
import Tag from '@atlaskit/tag'

import request from 'superagent'

class App extends Component {
  constructor() {
    super()
    this.state = {
      url: "",
      title: "",
      uploader: "",
      thumbnail: undefined,
      formats: []
    }

    this.onInputChanged = this.onInputChanged.bind(this)
    this.onDownloadBtnClicked = this.onDownloadBtnClicked.bind(this)
    this.formatItems = this.formatItems.bind(this)
  }

  onInputChanged(e) {
    this.setState({ 
      url: e.target.value 
    })

    console.log(e.target.value)
  }

  async onDownloadBtnClicked(e) {
    const res = await request
      .post('http://localhost:5000/api/video')
      .set('Content-Type', 'application/json')
      .send({"url": this.state.url})

      console.log(res.body)
      console.log(typeof(res.body))

    this.setState({...res.body})
  }

  formatItems(formats) {
    return <div className="Format-Items">
      { 
        formats.map(({id, url, note}) => (
          <a key={id} href={url}>
            <Tag text={note} />
          </a>
        ))
      }
    </div>
  }

  render() {
    const { formats, title, thumbnail } = this.state    
    let formatList = undefined
    let titleElem = undefined
    let thumbnailElem = undefined
    
    if (formats.length > 0) {
      formatList = this.formatItems(formats)
      titleElem = <PageHeader>{title}</PageHeader>
      thumbnailElem = <img style={{width: 550}} src={thumbnail} />
    }

    return (
      <div>
        <div className="Download-Container" >
          <TextField 
            className="Download-Input"
            isLabelHidden 
            autoFocus 
            onChange={this.onInputChanged} />
          <Button 
            className="Download-Btn" 
            appearance="primary"
            onClick={this.onDownloadBtnClicked} >
            Links
          </Button>
        </div>
        {titleElem}
        {thumbnailElem}
        {formatList}
      </div>
    )
  }
}

export default App;
